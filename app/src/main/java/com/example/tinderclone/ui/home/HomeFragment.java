package com.example.tinderclone.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.daprlabs.cardstack.SwipeDeck;
import com.example.tinderclone.Model.DeckAdapter;
import com.example.tinderclone.R;

import com.example.tinderclone.Retrofit.MyApiEndpointInterface;
import com.example.tinderclone.Retrofit.RetrofitInstance;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private SwipeDeck cardStack;
    ImageView ivLike,ivDisLike,ivReload;
    int lastSwipedPosition=0;
    private ArrayList<Result> mData =  new ArrayList();
    private ArrayList<Result> mTempData =  new ArrayList();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cardStack = (SwipeDeck) view.findViewById(R.id.swipeDesk);
        ivDisLike = view.findViewById(R.id.iconDislike);
        ivReload = view.findViewById(R.id.iconRefresh);
        ivLike = view.findViewById(R.id.iconLike);

        MyApiEndpointInterface service = RetrofitInstance.getInstance().create(MyApiEndpointInterface.class);
        Call<Response> call= service.getUserData();
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                Log.e("Response","SUCCESS");
                if(response.body() != null){
                    Log.e("Response",response.body().toString());
                    generateDataList();
                    mData.addAll((ArrayList<Result>) response.body().getResults());
                    mTempData.addAll(mData);
                }

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("Failure",t.getMessage());
            }
        });

        ivLike.setOnClickListener(v -> {
            cardStack.swipeTopCardRight(1000);
        });

        ivDisLike.setOnClickListener(v -> {
            cardStack.swipeTopCardLeft(1000);
        });
        Log.e("lastSwipedPosition",lastSwipedPosition+"");

        ivReload.setOnClickListener(v -> {
            Log.e("@@lastSwipedPosition",lastSwipedPosition+"");
            lastSwipedPosition = lastSwipedPosition - 1;
            if (lastSwipedPosition >= 0) {
                mTempData.clear();//clear all data

                for (int i = lastSwipedPosition; i < mData.size(); i++) {
                    mTempData.add(mData.get(i));
                }
                generateDataList();
                Log.e("mTempData",mTempData+"");

                if (lastSwipedPosition == 0){
                    Toast.makeText(getActivity(), "hide back button", Toast.LENGTH_SHORT).show();
                }
            } else {
                lastSwipedPosition = 0;
            }
        });
    }

    private void generateDataList() {


        final DeckAdapter adapter = new DeckAdapter(mTempData, getContext());

        cardStack.setAdapter(adapter);
        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {

            @Override
            public void cardSwipedLeft(int position) {
                Log.e("position",position+"");
                Toast.makeText(getActivity(), "Card Swiped Left", Toast.LENGTH_SHORT).show();
                lastSwipedPosition=position+1;
                Log.e("LeftlastSwipedPosition",lastSwipedPosition+"");
            }

            @Override
            public void cardSwipedRight(int position) {
                Log.e("position",position+"");
                Toast.makeText(getActivity(), "Card Swiped Right", Toast.LENGTH_SHORT).show();
                lastSwipedPosition=position+1;
                Log.e("RightlastSwipedPosition",lastSwipedPosition+"");
            }

            @Override
            public void cardsDepleted() {
                // this method is called when no card is present
                Toast.makeText(getActivity(), "No more courses present", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void cardActionDown() {
                // this method is called when card is swipped down.
                Log.e("TAG", "CARDS MOVED DOWN");
            }

            @Override
            public void cardActionUp() {
                Log.e("TAG", "CARDS MOVED UP");
            }
        });
    }


}