package com.example.tinderclone.ui.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tinderclone.Model.DatabaseHandler;
import com.example.tinderclone.MainActivity;
import com.example.tinderclone.R;
import com.example.tinderclone.ui.SignUp.SignUpFragment;

import java.util.List;


public class LoginFragment extends Fragment {

    EditText edEmail,edPassword;
    AppCompatButton btnLogin;
    TextView tvSignInHere,tvForgotPassword;
    String email,password;
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edEmail= view.findViewById(R.id.edtLoginEmail);
        edPassword= view.findViewById(R.id.edtLoginPassword);
        btnLogin = view.findViewById(R.id.btnLogin);
        tvSignInHere = view.findViewById(R.id.tvSignInHere);
        tvForgotPassword = view.findViewById(R.id.tvForgotPassword);

        btnLogin.setOnClickListener(v -> {
            email=edEmail.getText().toString();
            password = edPassword.getText().toString();
            Log.e("Email", email);
            Log.e("Password",password);

            DatabaseHandler dbHelper = new DatabaseHandler(getContext());
            String res=dbHelper.checkUserData(email,password);
            if(res == "0"){
                Toast.makeText(v.getContext(), "These Credentials do not match our records", Toast.LENGTH_LONG).show();
            }else{
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }

        });

        tvSignInHere.setOnClickListener(v ->{
            Log.e("pressed","Sign in here");
            Fragment signUpFragment = new SignUpFragment();
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.unAuthenticatedFragment, signUpFragment);
            fragmentTransaction.hide(new LoginFragment());
            fragmentTransaction.commit();
        });
    }
    public Fragment getVisibleFragment(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }
}