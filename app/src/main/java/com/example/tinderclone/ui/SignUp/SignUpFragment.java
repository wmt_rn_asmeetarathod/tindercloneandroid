package com.example.tinderclone.ui.SignUp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tinderclone.Model.DatabaseHandler;
import com.example.tinderclone.MainActivity;
import com.example.tinderclone.R;


public class SignUpFragment extends Fragment {

    EditText edtFirstName,edtLastName,edtEmail,edtPassword;
    String firstName,lastName,email,password;
    AppCompatButton btnSignUp;


    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtFirstName = view.findViewById(R.id.edtSignUpFirstName);
        edtLastName = view.findViewById(R.id.edtSignUpLastName);
        edtEmail = view.findViewById(R.id.edtSignUpEmail);
        edtPassword = view.findViewById(R.id.edtSignUpPassword);
        btnSignUp = view.findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(v ->{
            Log.e("btnSignUp","SignUp Pressed");
            firstName = edtFirstName.getText().toString();
            lastName = edtLastName.getText().toString();
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
             Log.e("firstName",firstName);
            Log.e("lastName",lastName);
            Log.e("email",email);
            Log.e("password",password);
            DatabaseHandler dbHelper = new DatabaseHandler(getContext());
            dbHelper.addUser(firstName,lastName,email,password);
            Toast.makeText(v.getContext(), "SignUp Successfully", Toast.LENGTH_LONG).show();
            Intent i = new Intent(getActivity(), MainActivity.class);
            startActivity(i);
        });

    }
}