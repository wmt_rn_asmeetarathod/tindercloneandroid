package com.example.tinderclone.Retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    // Trailing slash is needed
    public static final String BASE_URL = "https://randomuser.me/";
    public static Retrofit retrofit;


    public static Retrofit getInstance() {
        if(retrofit==null){
            Gson gson=new GsonBuilder().setLenient().create();
            retrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;

    }
}
