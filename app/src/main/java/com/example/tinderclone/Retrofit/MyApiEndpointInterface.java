package com.example.tinderclone.Retrofit;


import com.example.tinderclone.ui.home.Response;


import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MyApiEndpointInterface {

    // Request method and URL specified in the annotation

    @GET("/api/?results=5")
    Call<Response> getUserData();

}
