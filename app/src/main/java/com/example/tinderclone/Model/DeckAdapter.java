package com.example.tinderclone.Model;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tinderclone.R;
import com.example.tinderclone.ui.home.Result;

import java.util.ArrayList;

public class DeckAdapter extends BaseAdapter {

    private ArrayList<Result> userData;
    private Context context;

    public DeckAdapter(ArrayList<Result> userData, Context context) {
        this.userData = userData;
        this.context = context;

    }

    @Override
    public int getCount() {
        return userData.size();
    }

    @Override
    public Object getItem(int position) {
        return userData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v=convertView;
        if(v== null){
            v= LayoutInflater.from(parent.getContext()).inflate(R.layout.user_rv_item,parent,false);
        }
        Log.e("First NAME",userData.get(position).getName().getFirst());

        Glide.with(v).load(Uri.parse(userData.get(position).getPicture().getLarge())).into(((ImageView) v.findViewById(R.id.ivUserPhoto)));
        ((TextView) v.findViewById(R.id.tvUserName)).setText(userData.get(position).getName().getFirst());
        ((TextView) v.findViewById(R.id.tvUserEmail)).setText(userData.get(position).getEmail());
        ((TextView) v.findViewById(R.id.tvUserCountry)).setText(userData.get(position).getLocation().getCountry());


        return v;
    }
}
