package com.example.tinderclone.Model;

import android.net.Uri;

public class DataModal {

    private Uri userPhoto;
    private String userName;
    private String userEmail;
    private String userCountry;

    public DataModal(Uri userPhoto, String userName, String userEmail, String userCountry) {
        this.userPhoto = userPhoto;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userCountry = userCountry;
    }

    public Uri getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(Uri userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }
}
