package com.example.tinderclone.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseHandler  extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "tinderCloneDB";
    private static final String TABLE_USERS = "users";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRST_NAME = "firstName";
    private static final String KEY_LAST_NAME = "lastName";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    public DatabaseHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHandler(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS +
                "(" + KEY_ID + " INTEGER PRIMARY KEY," +
                      KEY_FIRST_NAME + " TEXT,"
                + KEY_LAST_NAME + " TEXT," + KEY_EMAIL + " TEXT," + KEY_PASSWORD + " TEXT"
                + ")";
        db.execSQL(CREATE_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);

        // Create tables again
        onCreate(db);
    }

    public void addUser(String firstName,String lastName,String email,String password) {

        Log.e("addUser firstName",firstName);
        Log.e("addUser lastName",lastName);
        Log.e("addUser email",email);
        Log.e("addUser password",password);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // Rows
        values.put(KEY_FIRST_NAME, firstName);
        values.put(KEY_LAST_NAME,lastName);
        values.put(KEY_EMAIL, email);
        values.put(KEY_PASSWORD, password);

        db.insert(TABLE_USERS, null, values);
        Log.e("SignUp Data", "Data Inserted");
        db.close();

        getAllUsers();
    }
    // code to get all contacts in a list view
    public void getAllUsers() {
        Log.e("getAllUsers"," IN getAllUsers()");
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.e("ID",cursor.getString(cursor.getColumnIndex(KEY_ID)));
                Log.e("FIRST NAME",cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME)));
                Log.e("LAST NAME",cursor.getString(cursor.getColumnIndex(KEY_LAST_NAME)));
                Log.e("EMAIL",cursor.getString(cursor.getColumnIndex(KEY_EMAIL)));
                Log.e("PASSWORD",cursor.getString(cursor.getColumnIndex(KEY_PASSWORD)));
            } while (cursor.moveToNext());
        }

    }

    public String checkUserData(String uName, String uPassword) {
        SQLiteDatabase db = this.getWritableDatabase();
        //Cursor cursor = db.query("tbl_users", null, "userEmail = ?", new String[]{uName}, null, null, null, null);

        String sql="select * from " +TABLE_USERS +" where "+ KEY_EMAIL +" ='"+uName+"'";
        Cursor cursor1=db.rawQuery(sql,null);

        if (cursor1 != null & cursor1.moveToFirst()) {
            String res=cursor1.getString(cursor1.getColumnIndex("id"));
            Log.e("res",res);
            cursor1.close();
            return res;
            //return 1;
        } else {
            return "0";
        }


    }
}
