package com.example.tinderclone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.tinderclone.ui.Login.LoginFragment;

public class UnAuthenticatedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_authenticated);

        LoginFragment loginFragment=new LoginFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(loginFragment, "LoginFragment");
        transaction.replace(R.id.unAuthenticatedFragment,loginFragment);
        transaction.commit();

    }
}